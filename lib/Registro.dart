import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:untitled/BackHomeInnovactive.dart';
import 'package:untitled/login.dart';


class Registro extends StatelessWidget{
  @override
  Widget build(BuildContext context) {    
    final boton = new InkWell(
      onTap:() => Navigator.push(context,
          MaterialPageRoute(builder: (context) => login())),
      child: new Container(
        margin: new EdgeInsets.only(
          top: 11.0,
          left: 22.0,
          right: 22.0,
        ),
        height: 50.0,
        width: 180.0,
        decoration: new BoxDecoration(
            borderRadius: new BorderRadius.circular(30.0),
            color: Color.fromARGB(255, 255, 2, 102)
        ),
        child: new Center(
          child: new Text(
            "Registrarse",
            style: const TextStyle(
              fontSize: 20.0,
              color: Colors.white,
              fontWeight: FontWeight.bold,
            ),),
        ),
      ),
    );
    return new Scaffold(
    body: new Stack(
      children: [
        new BackHomeInnovactive(),
        new Container(
          alignment: Alignment.center,
          margin: new EdgeInsets.only(
            top: 50.0
            ),
          child:  
          new Column(
            children: [
              new Text("Registro", style: const  TextStyle(
                fontSize: 55.0,
                color: Colors.white,
                fontWeight: FontWeight.bold
              ),
               ),
            ],
          ),
        ),
        new Container(
          color: Color.fromARGB(255, 18, 18, 18),
          
          margin: new EdgeInsets.only(
            top: 151.0
          ),
          padding: new EdgeInsets.only(
            top: 30.0,
            right: 125.0,
            left: 125.0,
            bottom: 30.0
          ),
          child: new Column(
            children: [
              new TextFormField(
                style: TextStyle(color: Colors.white),
                decoration: InputDecoration(
                contentPadding: EdgeInsets.all(20),
                labelText: "Nombre Completo",
                filled: true,
                fillColor: Color.fromARGB(255, 90, 90, 90),
                hintText: "Nombre",
                hintStyle: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                labelStyle: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                border: OutlineInputBorder(borderRadius: BorderRadius.circular(30)),
                icon: Icon(Icons.person, color: Colors.white,),
                )
              ),
              SizedBox(height: 40),
              new TextFormField(
                style: TextStyle(color: Colors.white),
                decoration: InputDecoration(
                contentPadding: EdgeInsets.all(20),
                labelText: "Correo Empresarial",
                filled: true,
                fillColor: Color.fromARGB(255, 90, 90, 90),
                hintText: "Correo Empresarial",              
                hintStyle: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                labelStyle: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                border: OutlineInputBorder(borderRadius: BorderRadius.circular(30)),
                icon: Icon(Icons.email, color: Colors.white,),
                )
              ),
              SizedBox(height: 40),
              new TextFormField(
                obscureText: true,
                decoration: InputDecoration(
                contentPadding: EdgeInsets.all(20),
                labelText: "Contraseña",
                filled: true,
                fillColor: Color.fromARGB(255, 90, 90, 90),
                hintText: "Contraseña",              
                hintStyle: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                labelStyle: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                border: OutlineInputBorder(borderRadius: BorderRadius.circular(30)),
                icon: Icon(Icons.password, color: Colors.white,),
                )
              ),
              SizedBox(height: 40),
              new TextFormField(  
                obscureText: true,
                decoration: InputDecoration(
                contentPadding: EdgeInsets.all(20),
                labelText: "Confirmar Contraseña",
                filled: true,
                fillColor: Color.fromARGB(255, 90, 90, 90),
                hintText: "Contraseña",
                hintStyle: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                labelStyle: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                border: OutlineInputBorder(borderRadius: BorderRadius.circular(30)),
                icon: Icon(Icons.password, color: Colors.white,),
                )
                  ),
              SizedBox(height: 40),
              boton,
            ],
          ),
        ),
      ],
    ) ,
  );   
  }
}