import 'package:flutter/material.dart';
import 'package:untitled/BackHomeInnovactive.dart';
import 'package:untitled/Registro.dart';

class login extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build+
  final boton = new InkWell(
    onTap:() => Navigator.push(context,
     MaterialPageRoute(builder: (context) => Registro())),
      child: new Container(
        margin: new EdgeInsets.only(
          top: 10.0,
          left: 20.0,
          right: 20.0,
        ),
        height: 50.0,
        width: 180.0,
        decoration: new BoxDecoration(
          borderRadius: new BorderRadius.circular(30.0),
          color: Color.fromARGB(255, 255, 2, 102)
        ),
        child: new Center(
          child: new Text(
            "Continuar",
            style: const TextStyle(
            fontSize: 20.0,
            color: Colors.white,
            fontWeight: FontWeight.bold,
          ),
          ),
        ),
      ),
      ); 
    return new Scaffold(
    body: new Stack(
      children: [
        new BackHomeInnovactive(),
        new Container(
          padding: new EdgeInsets.only(
            top: 30.0,
            right: 125.0,
            left: 125.0,
            bottom: 30.0,
          ),
          alignment: Alignment.center,
          margin: new EdgeInsets.only(
            top: 50.0
            ),
          child:  new Column(
            children: [
              new Text("Innovactive", 
              style: const  TextStyle(
                fontSize: 55.0,
                color: Colors.white,
                fontWeight: FontWeight.bold
              ),
               ),
              SizedBox(height: 40),
              new TextFormField(
                decoration: InputDecoration(
                contentPadding: EdgeInsets.all(20),
                filled: true,
                fillColor: Colors.white60,
                hintText: "Usuario",
                hintStyle: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
                border: OutlineInputBorder(borderRadius: BorderRadius.circular(30)),
                icon: Icon(Icons.person, color: Colors.white,),
                )
              ),
              SizedBox(height: 40),
              new TextFormField(
                decoration: InputDecoration(
                contentPadding: EdgeInsets.all(20),
                filled: true,
                fillColor: Colors.white60,
                hintText: "correo@email.com",
                hintStyle: TextStyle(color: Colors.black),
                border: OutlineInputBorder(borderRadius: BorderRadius.circular(30)),
                icon: Icon(Icons.email, color: Colors.white,),
                  )
              ),
              SizedBox(height: 40),
              new TextFormField(
                decoration: InputDecoration(
                contentPadding: EdgeInsets.all(20),
                filled: true,
                fillColor: Colors.white60,
                hintText: "Contraseña",
                hintStyle: TextStyle(color: Colors.black),
                border: OutlineInputBorder(borderRadius: BorderRadius.circular(30)),
                icon: Icon(Icons.block, color: Colors.white,),
                  ),
                obscureText: true,
              ),
              SizedBox(height: 40),
              boton,
              Container(
                margin: new EdgeInsets.only(
                  top: 52.0),
                color: Colors.white60,
                child:
                Align(
                alignment: Alignment.bottomCenter,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                    SizedBox(height: 40),
                    new InkWell(
              onTap:() => Navigator.push(context,
              MaterialPageRoute(builder: (context) => Registro())),
                child: new Container(
                  margin: new EdgeInsets.only(
                    left: 20.0,
                    right: 20.0,
                  ),
                  height: 40.0,
                  width: 180.0,
                  child: new Center(
                    child: new Text(
                      "Registrarse",
                      style: const TextStyle(
                      fontSize: 20.0,
                      color: Color.fromARGB(255, 255, 2, 102),
                      fontWeight: FontWeight.bold,
                    ),
                      ),
                    ),
                ),
                )
                    ]),
                  )
              )
            ],
          ),
        ),
      ],
    ) ,
  );   
  }
}


