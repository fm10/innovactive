import 'package:flutter/material.dart';

class BackHomeInnovactive extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Stack(
      children: [
        new FondoHome(),
      ],
    );
  }
}
class FondoHome extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage("assets/images/fondo_home_innovactive.png"),
          fit: BoxFit.cover,
        )
      ),
    );
  }

}