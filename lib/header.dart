import 'dart:ffi';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:untitled/pqrsf.dart';


class Header extends StatefulWidget {
  @override
  _HeaderState createState() => _HeaderState();
}

class _HeaderState extends State<Header> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Logo',
          style: TextStyle(color: Colors.pink),
        ),
        backgroundColor: Colors.black,
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.messenger),
            color: Colors.grey,
            // Mensaje del hover.
            tooltip: 'Mensaje',
            // Mensaje al hacer click.
            onPressed: () {
              ScaffoldMessenger.of(context)
                  .showSnackBar(const SnackBar(content: Text('Mensaje')));
            },
          ),
          IconButton(
            icon: const Icon(Icons.notifications),
            color: Colors.grey,
            tooltip: 'Mensaje',
            onPressed: () {
              ScaffoldMessenger.of(context)
                  .showSnackBar(const SnackBar(content: Text('Notificación')));
            },
          ),
          IconButton(
            icon: const Icon(Icons.search),
            color: Colors.grey,
            tooltip: 'Buscar',
            onPressed: () {
              showSearch(context: context, delegate: MySearchDelegate());
            },
          ),
          IconButton(
            icon: const Icon(Icons.menu),
            color: Colors.grey,
            tooltip: 'Menú',
            onPressed: () {
              ScaffoldMessenger.of(context)
                  .showSnackBar(const SnackBar(content: Text('Menú')));
            },
          ),
        ],
      ),
      // Menú de navegación.
      // Para colocarlo al lado derecho escribir: endDrawer: NavigationDrawner().
      // No deja colocarlo al lado derecho si hay algún ícono en la barra.
      drawer: NavigationDrawner(),
    );
  }
}

// =======|  BARRA DE BÚSQUEDA  |=======

class MySearchDelegate extends SearchDelegate {
  // Resultados que pueden ser relevantes.
  List<String> searchResults = [
    'Idea 1',
    'Idea 2',
    'Idea 3',
    'Idea 4',
  ];

  // Cerrar buscador.
  @override
  Widget? buildLeading(BuildContext context) => IconButton(
      icon: Icon(Icons.arrow_back),
      onPressed: () => close(context, null)); // Cerrar Buscador

  // Limpiar el buscador.
  @override
  List<Widget>? buildActions(BuildContext context) => [
        IconButton(
            icon: Icon(Icons.clear),
            onPressed: () {
              if (query.isEmpty) {
                close(context, null);
              } else {
                query = '';
              }
            })
      ];

  // Resultado del buscador.
  @override
  Widget buildResults(BuildContext context) => Center(
        child: Text(query,
            style: TextStyle(fontSize: 64, fontWeight: FontWeight.normal)),
      );

  // Sugerencias del buscador.
  @override
  Widget buildSuggestions(BuildContext context) {
    List<String> suggestions = searchResults.where((searchResult) {
      final result = searchResult.toLowerCase();
      final input = query.toLowerCase();

      return result.contains(input);
    }).toList();

    return ListView.builder(
      itemCount: suggestions.length,
      itemBuilder: (context, index) {
        final suggestion = suggestions[index];

        return ListTile(
          title: Text(suggestion),
          onTap: () {
            query = suggestion;
          },
        );
      },
    );
  }
}

// =======|  MENU  |=======

class HeaderMenu extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: const Text('Home'),
          backgroundColor: Colors.pink,
        ),
        drawer: const NavigationDrawner(),
      );
}

class NavigationDrawner extends StatelessWidget {
  const NavigationDrawner({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Drawer(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[buildHeader(context), buildMenuItems(context)],
          ),
        ),
      );

  Widget buildHeader(BuildContext context) => Material(
        color: Colors.pink,
        child: InkWell(
          onTap: () {
            Navigator.pop(context);

            // Agregar página de usuario.
            // Navigator.of(context).push(MaterialPageRoute(builder: (context) => const PaginaDePerfil()))
          },
          // Foto de perfil
          child: Container(
              padding: EdgeInsets.only(
                top: 24 + MediaQuery.of(context).padding.top,
                bottom: 24,
              ),
              child: Column(children: const [
                CircleAvatar(
                  radius: 52,
                  backgroundColor: Colors.black,
                ),
                SizedBox(height: 12),
                Text(
                  'Nombre de Usuario',
                  style: TextStyle(fontSize: 20, color: Colors.white),
                ),
                Text(
                  'Correo electrónico',
                  style: TextStyle(fontSize: 14, color: Colors.white),
                )
              ])),
        ),
      );

  // Elementos del menú
  Widget buildMenuItems(BuildContext context) => Container(
        padding: const EdgeInsets.all(24),
        color: Colors.black87,
        child: Wrap(
          runSpacing: 16, // Espacio vertical entre elementos.
          children: [
            ListTile(
                leading: const Icon(Icons.menu),
                title: const Text('Menú'),
                textColor: Colors.white,
                iconColor: Colors.pink,
                onTap: () {
                  Navigator.pop(context);

                  // Navigator.of(context).push(MaterialPageRoute(builder: (context) => const PaginaDePerfil()))
                }),
            const Divider(color: Colors.grey),
            ListTile(
              leading: const Icon(Icons.flag),
              title: const Text('Retos'),
              textColor: Colors.white,
              iconColor: Colors.pink,
              onTap: () {
                Navigator.pop(context);

                // Navigator.of(context).push(MaterialPageRoute(builder: (context) => const PaginaDePerfil()))
              },
            ),
            const Divider(color: Colors.grey),
            ListTile(
              leading: const Icon(Icons.lightbulb),
              title: const Text('Mis ideas'),
              textColor: Colors.white,
              iconColor: Colors.pink,
              onTap: () {
                Navigator.pop(context);

                // Navigator.of(context).push(MaterialPageRoute(builder: (context) => const PaginaDePerfil()))
              },
            ),
            const Divider(color: Colors.grey),
            ListTile(
              leading: const Icon(Icons.group_rounded),
              title: const Text('Postularme para comité o mentor'),
              textColor: Colors.white,
              iconColor: Colors.pink,
              onTap: () {
                Navigator.pop(context);

                // Navigator.of(context).push(MaterialPageRoute(builder: (context) => const PaginaDePerfil()))
              },
            ),
            const Divider(color: Colors.grey),
            ListTile(
              leading: const Icon(Icons.info),
              title: const Text('Acerca de nosotros'),
              textColor: Colors.white,
              iconColor: Colors.pink,
              onTap: () {
                Navigator.pop(context);

                // Navigator.of(context).push(MaterialPageRoute(builder: (context) => const PaginaDePerfil()))
              },
            ),
            const Divider(color: Colors.grey),
            ListTile(
                leading: const Icon(Icons.question_mark),
                title: const Text('Preguntas frecuentes'),
                textColor: Colors.white,
                iconColor: Colors.pink,
                onTap: () => {
                      Navigator.pop(context),
                      Navigator.of(context).pushReplacement(MaterialPageRoute(
                          builder: (context) => PreguntasFrecuentes())),
                    }),
            const Divider(color: Colors.grey),
            ListTile(
              leading: const Icon(Icons.settings_rounded),
              title: const Text('Ajustes'),
              textColor: Colors.white,
              iconColor: Colors.pink,
              onTap: () {
                Navigator.pop(context);

                // Navigator.of(context).push(MaterialPageRoute(builder: (context) => const PaginaDePerfil()))
              },
            ),
          ],
        ),
      );
}
