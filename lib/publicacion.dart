  import 'package:firebase_database/firebase_database.dart';
  import 'package:flutter/material.dart';
  import 'package:untitled/BackHomeInnovactive.dart';
  import 'package:firebase_core/firebase_core.dart';
  import 'package:untitled/Registro.dart';


  Future<bool> SaveData(String titulo, String contenido) async {  
    Firebase.initializeApp();
    try{
    await FirebaseDatabase.instance
    .reference()
    .child('ideas')
    .push()
    .set({
      'title': titulo,
      "content": contenido,
    });
    print("BIEN");
    return true;
    }catch(e){
      print(e);
      return false;
    }
    
  }

  class publicacion extends StatelessWidget{
    final TextEditingController _titulocontroller = new TextEditingController();
    final TextEditingController _contenidocontroller = new TextEditingController();

    @override
    Widget build(BuildContext context) {  
      final boton = new InkWell(
        child: new Container(
          margin: new EdgeInsets.only(
            top: 10.0,
            left: 20.0,
            right: 20.0,
          ),
          height: 50.0,
          width: 180.0,
          decoration: new BoxDecoration(
            borderRadius: new BorderRadius.circular(30.0),
            color: Color.fromARGB(255, 255, 2, 102)
          ),
          child: new Center(
            child: new Text(
              "Enviar",
              style: const TextStyle(
              fontSize: 20.0,
              color: Colors.white,
              fontWeight: FontWeight.bold,
            ),),          
          ),
        ),
        );    
      // TODO: implement build
      return  new Scaffold(
        body:
          new Stack(
          children: [
            new Container(
              color: Color.fromARGB(255, 18, 18, 18),
                  padding: new EdgeInsets.only(
                    top: 30.0,
                    bottom: 30.0,
                    left: 70.0,
                    right: 70.0,
                  ),
                  alignment: Alignment.center,
                  child:
                  new ListView(
                    shrinkWrap: true,
                    children: [
                      new Text("Formulario de Publicacion",
                      style: const TextStyle(
                        fontSize: 30.0,
                        color: Colors.white,
                        fontWeight: FontWeight.bold
                      ),
                      ),
                      SizedBox(height: 40),
                      new TextFormField(
                        validator: (String? dato){
                          if (dato !. isEmpty){
                            return 'Este campo es requerido';
                          }
                        },
                        controller: _titulocontroller,
                        style: TextStyle(color: Colors.white),
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.all(20),
                          labelText: "Título de tu idea*",
                          filled: true,
                          fillColor: Color.fromARGB(255, 36, 36, 36),
                          hintText: "Nombre",              
                          hintStyle: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                          labelStyle: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                          border: OutlineInputBorder(borderRadius: BorderRadius.circular(30)),
                          icon: Icon(Icons.person, color: Colors.white,),
                          )
                      ),
                      SizedBox(height: 40),
                      new TextFormField(
                        style: TextStyle(color: Colors.white),
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.all(20),
                          labelText: "Elige la categoría del reto a la que aplica",
                          filled: true,
                          fillColor: Color.fromARGB(255, 36, 36, 36),
                          hintText: "Elige la categoría del reto a la que aplica",
                          hintStyle: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                          labelStyle: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                          border: OutlineInputBorder(borderRadius: BorderRadius.circular(30)),
                          icon: Icon(Icons.person, color: Colors.white,),
                          )               
                      ),
                      SizedBox(height: 40),
                      new TextFormField(
                        maxLines: 4 ,
                        style: TextStyle(color: Colors.white),
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.all(20),
                          labelText: "Realiza una corta descripción del propósito de tu solución",
                          filled: true,
                          fillColor: Color.fromARGB(255, 36, 36, 36),
                          labelStyle: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                          border: OutlineInputBorder(borderRadius: BorderRadius.circular(30)),
                          icon: Icon(Icons.person, color: Colors.white,),
                          )
                      ),
                      SizedBox(height: 40),
                      new TextFormField(
                        maxLines: 4,
                        style: TextStyle(color: Colors.white),
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.all(20),
                          labelText: "¿Quiénes serían los principales beneficiados con esta solución?*",
                          filled: true,
                          fillColor: Color.fromARGB(255, 36, 36, 36),
                          labelStyle: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                          border: OutlineInputBorder(borderRadius: BorderRadius.circular(30)),
                          icon: Icon(Icons.person, color: Colors.white,),
                          )
                      ),
                      SizedBox(height: 40), 
                      new TextFormField(
                        maxLines: 4,
                        style: TextStyle(color: Colors.white),
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.all(20),
                          labelText: "Si tuvieras 30 segundos para describir tu idea, ¿Qué dirías? Trata de hacerlo como un tweet.",
                          filled: true,
                          fillColor: Color.fromARGB(255, 36, 36, 36),
                          hintText: "Escribe aca",              
                          hintStyle: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                          labelStyle: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                          border: OutlineInputBorder(borderRadius: BorderRadius.circular(30)),
                          icon: Icon(Icons.person, color: Colors.white,),
                          )
                      ),
                      SizedBox(height: 40),
                      new TextFormField(
                        style: TextStyle(color: Colors.white),
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.all(20),
                          labelText: "Crea etiquetas para tu idea",
                          filled: true,
                          fillColor: Color.fromARGB(255, 36, 36, 36),
                          hintText: "Nombre",
                          hintStyle: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                          labelStyle: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                          border: OutlineInputBorder(borderRadius: BorderRadius.circular(30)),
                          icon: Icon(Icons.person, color: Colors.white,),
                          )
                      ),
                      SizedBox(height: 40),
                      ElevatedButton(
                        onPressed: ()  async {
                          SaveData("Text1", "Text2");
                        },                       
                        child: const Text('Enviar'))
                ],
                  ),
                  ),
            ], 
          ),
          );
      throw UnimplementedError();
    }

  }
