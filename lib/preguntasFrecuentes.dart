import 'package:flutter/material.dart';
import 'header.dart';
import 'footer.dart';

class PreguntasFrecuentes extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Stack(
        // Intenté agregar los widgets a esta vista pero quedan debajo.
        children: [
          new Footer(),
          new Header(),
          new Column(
            children: [titleSection, itemPregunta],
          )
        ],
      ),
    );
  }
}

// Título de "¿? Preguntas Frecuentes"
Widget titleSection = Container(
    padding: const EdgeInsets.only(
      top: 32.0,
      right: 32.0,
      bottom: 8.0,
      left: 32.0,
    ),
    color: Colors.black,
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        const Text(
          '¿?',
          style: TextStyle(
              fontSize: 45.0, fontWeight: FontWeight.w400, color: Colors.pink),
        ),
        const Text(
          'Preguntas frecuentes',
          style: TextStyle(
            fontSize: 22.0,
            fontWeight: FontWeight.w600,
            color: Colors.pink,
          ),
        ),
      ],
    ));

// Prueba que hice copiando el código pero que luego no entendí qué hacía. xD

// Widget itemPregunta() {
//   return ExpansionPanelList(
//       expansionCallback: (int index, bool isExpanded) {
//         setState(() {
//           _data[index].isExpanded = !isExpanded;
//         });
//       },
//       children: _data.map<ExpansionPanel>((Item item) {
//         return ExpansionPanel(
//           headerBuilder: (BuildContext contex, bool isExpanded) {
//             return ListTile(
//               title: Text(item.headerValue),
//             );
//           },
//           body: ListTile(
//             title: Text(item.expandedValue),
//             subtitle:
//                 const Text('To delete this panel, tap the trash can icon'),
//             trailing: const Icon(Icons.delete),
//             onTap: () {
//               setState(() {
//                 _data.removeWhere((Item currentItem) => item == currentItem);
//               });
//             },
//           ),
//           isExpanded: item.isExpanded,
//         );
//       }).toList());
// }

// Widget itemPregunta() {
//   return ExpansionPanel(
//     headerBuilder: (context, isOpen) {
//       return Text('Pregunta prueba');
//     },
//     body: Text('Abierto'),
//     isExpanded:
//   )
// }

// Una sola pregunta sin funcionalidad desplegable.
Widget itemPregunta = Container(
  padding: const EdgeInsets.only(
    top: 8.0,
    right: 32.0,
    bottom: 32.0,
    left: 32.0,
  ),
  color: Colors.black,
  child: Row(
    mainAxisAlignment: MainAxisAlignment.start,
    children: [
      Expanded(
          child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: const EdgeInsets.only(bottom: 15),
            child: Text(
              'Pregunta General',
              style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 20.0,
                  color: Color.fromARGB(255, 191, 84, 210)),
            ),
          ),
          Text(
              "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 13.0,
                  fontWeight: FontWeight.w300))
        ],
      )),
      Icon(
        Icons.keyboard_arrow_down_rounded,
        color: Color.fromARGB(255, 191, 84, 210),
        size: 47.0,
      )
    ],
  ),
);
