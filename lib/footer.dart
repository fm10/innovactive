import 'package:flutter/material.dart';

class Footer extends StatefulWidget {
  @override
  _FooterState createState() => _FooterState();
}

class _FooterState extends State<Footer> {
  int _selectedIndex = 0;

  // Lista con las vistas. Una por cada botón.
  List<Widget> _widgetOptions = [
    VistaHome(),
    VistaDos(),
    VistaTres(),
    VistaAgregar()
  ];

  //Estado del Index para asociarlo con las vistas.
  void _onItemTap(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // Visualización de la vista en el centro.
      body: Center(child: _widgetOptions.elementAt(_selectedIndex)),
      // Barra de navegación con los botones.
      bottomNavigationBar: BottomNavigationBar(
        selectedItemColor: Colors.pink,
        unselectedItemColor: Colors.grey,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
            backgroundColor: Colors.black,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.flag),
            label: 'Flag',
            backgroundColor: Colors.black,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.lightbulb),
            label: 'Light bulb',
            backgroundColor: Colors.black,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.add),
            label: 'Add',
            backgroundColor: Colors.black,
          ),
        ],
        currentIndex: _selectedIndex,
        onTap: _onItemTap,
      ),
    );
  }
}

// =======|  VISTAS  |=======

class VistaHome extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
          child: Text(
        'Home',
        style: TextStyle(fontSize: 30.0),
      )),
    );
  }
}

class VistaDos extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
          child: Text(
        'Página Dos',
        style: TextStyle(fontSize: 30.0),
      )),
    );
  }
}

class VistaTres extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
          child: Text(
        'Página Tres',
        style: TextStyle(fontSize: 30.0),
      )),
    );
  }
}

class VistaAgregar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
          child: Text(
        'Página Agregar',
        style: TextStyle(fontSize: 30.0),
      )),
    );
  }
}
