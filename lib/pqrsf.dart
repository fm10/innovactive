import 'package:flutter/material.dart';
import 'package:untitled/footer.dart';
import 'header.dart';

class PreguntasFrecuentes extends StatefulWidget {
  @override
  State<PreguntasFrecuentes> createState() => _PreguntasFrecuentes();
}

// Título de la vista y preguntas insertadas.
class _PreguntasFrecuentes extends State<PreguntasFrecuentes> {
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData.dark(),
      home: Scaffold(
        body: new Stack(
          children: [
            // Traté de integrar el header pero queda debajo.
            Header(),
            // Título de "¿? Preguntas Frecuentes".
            new Container(
                padding: const EdgeInsets.only(
                  top: 32.0,
                  right: 32.0,
                  bottom: 8.0,
                  left: 32.0,
                ),
                color: Colors.black,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    const Text(
                      '¿?',
                      style: TextStyle(
                          fontSize: 45.0,
                          fontWeight: FontWeight.w600,
                          color: Colors.pink),
                    ),
                    const Text(
                      'Preguntas frecuentes',
                      style: TextStyle(
                        fontSize: 22.0,
                        fontWeight: FontWeight.w600,
                        color: Colors.pink,
                      ),
                    ),
                  ],
                )),
            // Preguntas insertadas.
            SingleChildScrollView(child: ExpansionWid())
          ],
        ),
      ),
    );
  }
}

// =======|  PREGUNTAS  |=======

class ExpansionWid extends StatefulWidget {
  const ExpansionWid({
    Key? key,
  }) : super(key: key);

  @override
  State<ExpansionWid> createState() => _ExpansionWidState();
}

class _ExpansionWidState extends State<ExpansionWid> {
  // Contenido de la pregunta.
  static const loremIpsum =
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.";

  // Objeto preguntas.
  List<Preguntas> pregunta = [
    // Preguntas(id, título de la pregunta, contenido de la pregunta, si comienza desplegado o no).
    Preguntas(1, "Pregunta General", loremIpsum, false),
    Preguntas(2, "Pregunta General", loremIpsum, false),
    Preguntas(3, "Pregunta General", loremIpsum, false),
    Preguntas(4, "Pregunta General", loremIpsum, false),
    Preguntas(5, "Pregunta General", loremIpsum, false),
  ];

  @override
  Widget build(BuildContext context) {
    // Para que se abra más de una pregunta a la vez descomentar 'isExpanded' y cambiar los return de 'ExpansionPanelList.radio' a 'ExpansionPanelList', y 'ExpansionPanelRadio' por 'ExpansionPanel'
    return ExpansionPanelList.radio(
      dividerColor: Colors.grey,
      expansionCallback: (panelIndex, isExpanded) {
        setState(() {
          pregunta[panelIndex].isExpanded = !isExpanded;
        });
      },
      children: pregunta.map((car) {
        return ExpansionPanelRadio(
            canTapOnHeader: true,
            value: car.id,
            // isExpanded: car.isExpanded,
            headerBuilder: (bc, status) {
              return Container(
                child: Container(
                    padding: EdgeInsets.all(10),
                    child: Text(
                      car.name,
                      style:
                          TextStyle(color: Color.fromARGB(255, 191, 84, 210)),
                    )),
              );
            },
            body: Container(
              padding: EdgeInsets.all(10),
              height: 100,
              child: Text(
                car.description,
              ),
            ));
      }).toList(),
    );
  }
}

// Clase para instanciar preguntas.
class Preguntas {
  int id;
  String name;
  String description;
  bool isExpanded;

  Preguntas(this.id, this.name, this.description, this.isExpanded);
}
